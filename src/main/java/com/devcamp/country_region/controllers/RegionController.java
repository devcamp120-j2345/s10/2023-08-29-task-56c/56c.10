package com.devcamp.country_region.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.country_region.models.Region;
import com.devcamp.country_region.services.RegionService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class RegionController {
    @Autowired
    RegionService regionService;

    @GetMapping("/getRegionByRegionCode")
    public Region getRegionByRegionCode(@RequestParam String regionCode) {
        Region result = regionService.getRegion(regionCode);

        return result;
    }
}
