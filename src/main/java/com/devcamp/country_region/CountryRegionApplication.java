package com.devcamp.country_region;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CountryRegionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CountryRegionApplication.class, args);
	}

	
}
