package com.devcamp.country_region.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.country_region.models.Country;

@Service
public class CountryService {
    @Autowired
    RegionService regionService;

    private Country vietNam = new Country("VN", "Việt Nam");
    private Country japan = new Country("JP", "Nhật Bản");
    private Country usa = new Country("USA", "Hoa Kỳ");

    public ArrayList<Country> getCountryList() {
        ArrayList<Country> countries = new ArrayList<>();

        vietNam.setRegions(regionService.getVietNamRegions());
        japan.setRegions(regionService.getJapanRegions());
        usa.setRegions(regionService.getUSARegions());

        countries.add(vietNam);
        countries.add(japan);
        countries.add(usa);

        return countries;
    }

    public Country getCountry(String countryCode) {
        ArrayList<Country> countries = new ArrayList<>();

        vietNam.setRegions(regionService.getVietNamRegions());
        japan.setRegions(regionService.getJapanRegions());
        usa.setRegions(regionService.getUSARegions());

        countries.add(vietNam);
        countries.add(japan);
        countries.add(usa);

        for (Country country : countries) {
            if(country.getCountryCode().equals(countryCode)) {
                return country;
            }
        }

        return null;
    }
}
